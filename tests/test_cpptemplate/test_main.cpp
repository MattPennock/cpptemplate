#include "main.h"
#include <gtest/gtest.h>

using namespace std;

void STRING_ASSERT(string received, string expected)
{
    ASSERT_EQ(received.length(), expected.length());
    for (int i = 0; i < received.length(); i++)
    {
        ASSERT_EQ(received[i], expected[i]);
    }
}

TEST(cpptemplate, make_string_says_hi)
{
    string received = cpptemplate_header::make_string();
    string expected = "Hello world!";
    STRING_ASSERT(received, expected);
    // EXPECT_STREQ(cpptemplate_header::make_string(), "Hello world!");
}