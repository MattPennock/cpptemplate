# C++ Template
Example Template for C++ project using the following toolchain:

Build System: CMake

Compiler: MinGW g++

Test Framework: Google gtest

Coverage Tool: gcovr

## Installing Dependencies:
- [CMake](https://cmake.org/download/) download installer
- [MinGW](https://sourceforge.net/projects/mingw/) download installer
- [Google gtest](https://github.com/google/googletest/releases) managed through CMake
- gcovr
  - Depends on [Python](https://www.python.org/downloads/)
  - Often performed in a local virtual environment of Python to protect your global installation
    ```bash
    pip install virtualenv # virtual environment manager for Python
    setx PIP_REQUIRE_VIRTUALENV True # protects your global environment from accidental installs
    virtualenv venv # generates a `venv` subfolder for the virtual environment
    "venv/scripts/activate" # activates said `venv`
    pip install gcovr
    ```

## Building your project:
1. Generate buildsystem metafiles through cmake.
    - The generator must be specified to use MinGW's gcc or g++ this way if that is of interest for future toolchain considerations.
    - This defaults to using a debug build type, which will build Google Test and the written test suite unoptimized.
    ```bash
    # ./
    cmake -G "MinGW Makefiles" -B build -S . -DCMAKE_BUILD_TYPE=Debug
    # cmake -G "MinGW Makefiles" -B build -S . -DCMAKE_BUILD_TYPE=Release # release build type
    ```
2. Build your source and dependencies:
    ```bash
    # ./
    cmake --build build
    ```


3. Generate coverage html and xml report and validate coverage:
    ```bash
    # ./
    mkdir .test_results # generate and .gitignore whatever subdirectory to place results in
    gcovr  # configured by ./gcovr.cfg
    ```

## Usage
Run your program or test suite:
```bash
# ./
"build/src/cpptemplate/cpptemplate.exe" # runs main
"build/test/test_cpptemplate/test_cpptemplate.exe" # runs gtest suite
```

# Contributing
See [Contributing.MD](Contributing.MD)